package nl.tomcomp.rest;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import nl.tomcomp.rest.exception.InvalidResponseException;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

/**
 * Representatie van een Rest resource waarop methoden kunnen worden losgelaten
 * (POST, GET, ...)
 * 
 * @author tom.kirchjunger
 * 
 */
public class RestResource {

	protected final String uri;

	protected String contentType;

	public RestResource(String uri) {
		this.uri = uri;
	}

	RestResource(String uri, String contentType) {
		this.uri = uri;
		this.contentType = contentType;
	}

	public RestResource accept(String contentType) {
		this.contentType = contentType;
		return this;
	}

	@SuppressWarnings("unchecked")
	public <T> T get(final Class<T> clazz) throws InvalidResponseException {
		try {
			return (T) RestClient.get()
					.doGet(uri, new ArrayList<NameValuePair>()).toString();
		} catch (ClientProtocolException e) {
		} catch (IOException e) {
		} catch (URISyntaxException e) {
		} catch (JSONException e) {
		}
		return null;
	}

}
