package nl.tomcomp.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class HttpClientHelper {

	public static URI constructUrlWithGetParameters(String uri,
			final List<NameValuePair> nameValuePairs) throws URISyntaxException {
		if (nameValuePairs == null || nameValuePairs.isEmpty())
			return new URI(uri);

		if (!uri.endsWith("?"))
			uri += "?";
		String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
		return new URI(uri + paramString);
	}
}
