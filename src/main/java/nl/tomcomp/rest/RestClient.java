package nl.tomcomp.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import nl.tomcomp.rest.exception.InvalidResponseException;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Eenvoudige REST client
 * 
 * @author tom.kirchjunger
 * 
 */
public class RestClient {

	private final HttpClient httpClient;

	private static RestClient instance = null;

	private RestClient() {
		httpClient = new DefaultHttpClient();
	}

	public static RestClient get() {
		if (instance == null)
			instance = new RestClient();

		return instance;
	}

	/**
	 * Sluit alle verbindingen en gebruikte resources netjes af
	 */
	public void shutdownPolite() {
		httpClient.getConnectionManager().shutdown();
	}

	public void doPostNoResult(final String uri, final JSONObject jsonObject)
			throws ClientProtocolException, URISyntaxException, IOException,
			JSONException, InvalidResponseException {
		HttpResponse response = executePost(uri, jsonObject);
		if (response.getStatusLine() == null
				|| response.getStatusLine().getStatusCode() != 200)
			throw new InvalidResponseException(String.format(
					"Onverwachte http status - %s: %s", response
							.getStatusLine().getStatusCode(), response
							.getStatusLine().getReasonPhrase()));
	}

	public JSONObject doPost(final String uri, final JSONObject jsonObject)
			throws ClientProtocolException, URISyntaxException, IOException,
			JSONException {
		HttpResponse response = executePost(uri, jsonObject);
		if (response != null)
			return getJson(response);
		return null;
	}

	public JSONObject doGet(final String uri, List<NameValuePair> nameValuePairs)
			throws ClientProtocolException, IOException, URISyntaxException,
			JSONException {
		HttpResponse response = executeGet(uri, nameValuePairs);
		if (response != null)

			return getJson(response);
		return null;
	}

	/**
	 * 
	 * @param response
	 *            not null
	 * @return
	 * @throws JSONException
	 * @throws IOException
	 */
	protected JSONObject getJson(final HttpResponse response)
			throws JSONException, IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				response.getEntity().getContent(), "UTF-8"));
		StringBuilder builder = new StringBuilder();
		for (String line = null; (line = reader.readLine()) != null;) {
			builder.append(line).append("\n");
		}
		JSONTokener tokener = new JSONTokener(builder.toString());
		return new JSONObject(tokener);
	}

	protected HttpResponse executePost(final String uri,
			final JSONObject jsonObject) throws URISyntaxException,
			ClientProtocolException, IOException {
		return executePost(new URI(uri), jsonObject);
	}

	protected HttpResponse executePost(final URI uri,
			final JSONObject jsonObject) throws ClientProtocolException,
			IOException {
		HttpPost post = prepairHttpPost(uri, jsonObject);
		HttpResponse response = httpClient.execute(post);
		return response;
	}

	protected HttpResponse executeGet(final String uri,
			final List<NameValuePair> nameValuePairs)
			throws ClientProtocolException, IOException, URISyntaxException {
		HttpGet get = prepairHttpGet(uri, nameValuePairs);
		HttpResponse response = httpClient.execute(get);
		return response;
	}

	/**
	 * 
	 * @param uri
	 * @param jsonObject
	 *            not null
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private HttpPost prepairHttpPost(final URI uri, final JSONObject jsonObject)
			throws UnsupportedEncodingException {
		HttpPost post = new HttpPost(uri);
		StringEntity entity = new StringEntity(jsonObject.toString(), "UTF-8");
		entity.setContentType("application/json");
		post.setEntity(entity);
		return post;
	}

	private HttpGet prepairHttpGet(final String uri,
			final List<NameValuePair> nameValuePairs) throws URISyntaxException {
		return new HttpGet(HttpClientHelper.constructUrlWithGetParameters(uri,
				nameValuePairs));
	}
}
