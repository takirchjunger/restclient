package nl.tomcomp.rest.exception;

public class InvalidResponseException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidResponseException(String detailMessage) {
		super(detailMessage);
	}

	public InvalidResponseException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

}
