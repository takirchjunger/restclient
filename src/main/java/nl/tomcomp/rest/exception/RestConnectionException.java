package nl.tomcomp.rest.exception;

public class RestConnectionException extends Exception {

	private static final long serialVersionUID = 1L;

	public RestConnectionException(String detailMessage) {
		super(detailMessage);
	}

	public RestConnectionException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

}
